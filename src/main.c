/*
 * This is the starting point for the EN0572 assignment 2011.
 * 
 * This program illustrates the use of some uC/OS-II functions and some
 * board support functions. It illustrates some programming techniques that
 * may be useful in developing a solution to the assignment problem.
 * 
 * However, most of the code given here in main.c  will have to be modified in 
 * some way in order to be useful in the final solution.
 *
 * You should NOT modify any of the code in bsp or Micrium.
 *
 * N.B. The program as it stands is broken. The LED tasks interfere with
 * each other when sharing the LCD. A first step towards a working program
 * would be to introduce a semaphore to provide mutually exclusive access to 
 * the LCD. A more sophisticated solution will introduce a dedicated LCD
 * task and require tasks that wish to use the LCD to do so by communicating 
 * with the LCD task using a bounded buffer.
 */



#include <stdbool.h>
#include <ucos_ii.h>
#include <osutils.h>
#include <bsp.h>
#include <leds.h>
#include <buttons.h>
#include <lcd.h>

enum delayConstants {ADJUST_DELAY  = 10,   MIN_DELAY = 10,  
                     DEFAULT_DELAY = 500, MAX_DELAY  = 5000};


/*
*********************************************************************************************************
*                                            APPLICATION TASK PRIORITIES
*********************************************************************************************************
*/

#define  APP_TASK_BUTTONS_PRIO                    4
#define  APP_TASK_LINK_PRIO                       6
#define  APP_TASK_CONNECT_PRIO                    8

/*
*********************************************************************************************************
*                                            APPLICATION TASK STACKS
*********************************************************************************************************
*/

#define  APP_TASK_BUTTONS_STK_SIZE              256
#define  APP_TASK_LINK_STK_SIZE                 256
#define  APP_TASK_CONNECT_STK_SIZE              256

static OS_STK appTaskButtonsStk[APP_TASK_BUTTONS_STK_SIZE];
static OS_STK appTaskLinkStk[APP_TASK_LINK_STK_SIZE];
static OS_STK appTaskConnectStk[APP_TASK_CONNECT_STK_SIZE];

/*
*********************************************************************************************************
*                                            APPLICATION TASK FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static void appTaskButtons(void *pdata);
static void appTaskLinkLed(void *pdata);
static void appTaskConnectLed(void *pdata);


/*
*********************************************************************************************************
*                                           OTHER LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*/

void incDelay(uint16_t *, uint8_t);
void decDelay(uint16_t *, uint8_t);


/*
*********************************************************************************************************
*                                            GLOBAL VARIABLE DEFINITIONS
*********************************************************************************************************
*/

static bool linkLedFlashing = false;
static bool connectLedFlashing = false;
static uint16_t linkLedDelay = 500;
static uint16_t connectLedDelay = 500;



/*
*********************************************************************************************************
*                                            GLOBAL FUNCTION DEFINITIONS
*********************************************************************************************************
*/

int main() {

  /* Initialise the board support package */
  bspInit();
  
  /* Initialise the OS */
  OSInit();                                                   

  /* Create the tasks */
  OSTaskCreate(appTaskButtons,                               
               (void *)0,
               (OS_STK *)&appTaskButtonsStk[APP_TASK_BUTTONS_STK_SIZE - 1],
               APP_TASK_BUTTONS_PRIO);
  
  OSTaskCreate(appTaskLinkLed,                               
               (void *)0,
               (OS_STK *)&appTaskLinkStk[APP_TASK_LINK_STK_SIZE - 1],
               APP_TASK_LINK_PRIO);
  
  OSTaskCreate(appTaskConnectLed,                               
               (void *)0,
               (OS_STK *)&appTaskConnectStk[APP_TASK_CONNECT_STK_SIZE - 1],
               APP_TASK_CONNECT_PRIO);

  
  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/*
*********************************************************************************************************
*                                            APPLICATION TASK DEFINITIONS
*********************************************************************************************************
*/


static void appTaskButtons(void *pdata) {
  uint32_t currentState;
  uint32_t oldState;
  
  while (true) {
    currentState = buttonsRead();
    if (currentState != oldState) {
      oldState = currentState;
      if (updateButtonState(currentState, BUT_1) == B_PRESSED_RELEASED) {
        linkLedFlashing = !linkLedFlashing;
      }
      if (updateButtonState(currentState, BUT_2) == B_PRESSED_RELEASED) {
        connectLedFlashing = !connectLedFlashing;
      }
      if (linkLedFlashing) {
        if (updateButtonState(currentState, JS_UP) == B_PRESSED_RELEASED) {
          decDelay(&linkLedDelay, ADJUST_DELAY);
        } else if (updateButtonState(currentState, JS_DOWN) == B_PRESSED_RELEASED){
          incDelay(&linkLedDelay, ADJUST_DELAY);
        }
      }
      if (connectLedFlashing) {
        if (updateButtonState(currentState, JS_RIGHT) == B_PRESSED_RELEASED) {
          decDelay(&connectLedDelay, ADJUST_DELAY);
        } else if (updateButtonState(currentState, JS_LEFT) == B_PRESSED_RELEASED){
          incDelay(&connectLedDelay, ADJUST_DELAY);
        }
      }
    }
    OSTimeDly(10);
  }
}

static void appTaskLinkLed(void *pdata) {
  
  /* Start the OS ticker -- must be done in the highest priority task */
  osStartTick();
  
  /* Task main loop */
  while (true) {
    if (linkLedFlashing) {
      ledToggle(USB_LINK_LED);
    }
  
    lcdSetTextPos(2, 1);
    lcdWrite("(LINK) F:%s", (linkLedFlashing ? "ON " : "OFF"));
    lcdSetTextPos(2, 2);
    lcdWrite("       D:%5d", linkLedDelay);

    OSTimeDly(linkLedDelay);
  }
}

static void appTaskConnectLed(void *pdata) {

  while (true) {
    OSTimeDly(connectLedDelay);
    if (connectLedFlashing) {
    ledToggle(USB_CONNECT_LED);
    }

    lcdSetTextPos(2, 4);
    lcdWrite("(CNCT) F:%s",(connectLedFlashing ? "ON " : "OFF"));
    lcdSetTextPos(2, 5);
    lcdWrite("       D:%5d", connectLedDelay);
 
    OSTimeDly(connectLedDelay);
   } 
}


static void incDelay(uint16_t *delay, uint8_t inc) {
  if (*delay <= MAX_DELAY - inc) {
    *delay += inc;
  }
  else {
    *delay = MAX_DELAY;
  }
}

static void decDelay(uint16_t *delay, uint8_t dec) {
  if (*delay >= MIN_DELAY + dec) {
    *delay -= dec;
  }
  else {
    *delay = MIN_DELAY;
  }
}